#include <stdio.h>
#include <stdlib.h>

#include "menu.h"
#include "abb.h"

int readFromKeyBoard()
{
    int value;
    int ch;
    scanf(" %d", &value);
    while ((ch = getchar()) != '\n')
        ;
    return value;
}

int main(void)
{

    int loopFlag = 1;
    int value;
    Abb * bTree;
    Abb * temp;

    while (loopFlag)
    {
        int opt = getMenuOption("Criar uma árvore binária de busca",
                                "Inserir um elemento",
                                "Procurar/Recuperar um elemento",
                                "Remover um elemento",
                                "Recuperar o sucessor de um elemento",
                                "Percorrer/Imprimir a árvore em pré-ordem",
                                "Percorrer/Imprimir a árvore em pós-ordem",
                                "Percorrer/Imprimir a árvore em ordem",
                                NULL);
        printf("\nopt chose ----> %d\n", opt);

        switch (opt)
        {
        case 1:
            bTree = abb_cria();
            break;
        case 2:
            value = readFromKeyBoard();
            bTree = abb_insere(bTree, value);
            print_tree(bTree, 0);
            break;
        case 3:
            printf("\nDigite o valor a ser procurado:\n");
            temp = abb_busca(bTree, readFromKeyBoard());
            if (temp != NULL) {
                print_tree(temp, 0);
            } else {
                printf("\nValor não encontrado\n");
            }
            break;
        case 4:
            printf("\nDigite valor a ser removido:\n");
            temp = abb_retira(bTree, readFromKeyBoard());
            if (temp == NULL) {
                printf("\nValor não encontrado\n");
                continue;
            }
            bTree = temp;
            print_tree(bTree, 0);
            break;
        case 5:
            printf("\nDigite o elemento o qual deseja buscar o sucessor?\n");
            temp = abb_busca(bTree, readFromKeyBoard());
            if (temp == NULL) {
                printf("\nValor não encontrado\n");
                continue;
            }
            print_tree(abb_prox(temp),0);
            break;
        case 6:
            abb_preorder(bTree);
            break;
        case 7:
            abb_postorder(bTree);
            break;
        case 8:
            abb_inorder(bTree);
            break;
        default:
            loopFlag = 0;
            break;
        }
    }
    getchar();
    return 0;
}