#include <stdio.h>
#include <stdlib.h>
#include "abb.h"

struct abb
{
	int chave;
	Abb *pai;
	Abb *esq;
	Abb *dir;
};

static Abb *cria_filho(Abb *pai, int val)
{
	Abb *no = (Abb *)malloc(sizeof(Abb));
	no->chave = val;
	no->pai = pai;
	no->esq = no->dir = NULL;
	return no;
}

Abb *abb_cria(void)
{
	return NULL;
}

Abb *abb_insere(Abb *r, int val)
{
	if (r == NULL)
		return cria_filho(r, val);
	else if (val < r->chave)
	{
		if (r->esq == NULL)
			r->esq = cria_filho(r, val);
		else
			r->esq = abb_insere(r->esq, val);
	}
	else if (val > r->chave)
	{
		if (r->dir == NULL)
			r->dir = cria_filho(r, val);
		else
			r->dir = abb_insere(r->dir, val);
	}
	return r;
}

Abb *abb_busca(Abb *r, int v)
{
	if (r == NULL)
		return NULL;
	else if (v < r->chave)
		return abb_busca(r->esq, v);
	else if (v > r->chave)
		return abb_busca(r->dir, v);
	else
		return r;
}

Abb *abb_min(Abb *r)
{
	if (r == NULL)
		return NULL;
	while (r->esq != NULL)
		r = r->esq;
	return r;
}

Abb *abb_prox(Abb *r)
{
	if (r == NULL)
		return NULL;
	else if (r->dir != NULL)
		return abb_min(r->dir);
	else
	{
		Abb *p = r->pai;
		while (p != NULL && r == p->dir)
		{
			r = p;
			p = p->pai;
		}
		return p;
	}
}

Abb *abb_retira(Abb *r, int chave)
{
	if (r == NULL)
		return NULL;
	else if (chave < r->chave)
		r->esq = abb_retira(r->esq, chave);
	else if (chave > r->chave)
		r->dir = abb_retira(r->dir, chave);
	else
	{
		if (r->esq == NULL && r->dir == NULL)
		{
			free(r);
			r = NULL;
		}
		else if (r->esq == NULL)
		{
			Abb *t = r;
			r = r->dir;
			r->pai = t->pai;
			free(t);
		}
		else if (r->dir == NULL)
		{
			Abb *t = r;
			r = r->esq;
			r->pai = t->pai;
			free(t);
		}
		else
		{
			Abb *sucessor = r->dir;
			while (sucessor->esq != NULL)
				sucessor = sucessor->esq;
			r->chave = sucessor->chave;
			sucessor->chave = chave;
			if (sucessor->pai->esq == sucessor)
				sucessor->pai->esq = sucessor->dir;
			else
				sucessor->pai->dir = sucessor->dir;
			if (sucessor->dir != NULL)
				sucessor->dir->pai = sucessor->pai;
			free(sucessor);
		}
	}
	return r;
}

void abb_inorder(Abb *root)
{
	if (root == NULL)
		return;

	abb_inorder(root->esq);

	printf("%d ", root->chave);
	abb_inorder(root->dir);
}

void abb_preorder(Abb *root)
{
	if (root == NULL)
		return;

	printf("%d ", root->chave);
	abb_preorder(root->esq);
	abb_preorder(root->dir);
}

void abb_postorder(Abb *root)
{
	if (root == NULL)
		return;

	abb_postorder(root->esq);
	abb_postorder(root->dir);

	printf("%d ", root->chave);
}

void padding(char ch, int n)
{
	int i;

	for (i = 0; i < n; i++)
		putchar(ch);
}

void print_tree(Abb *root, int level)
{
	int i;

	if (root == NULL)
	{
		padding('\t', level);
		puts("~");
	}
	else
	{
		print_tree(root->dir, level + 1);
		padding('\t', level);
		printf("%d\n", root->chave);
		print_tree(root->esq, level + 1);
	}
}