#ifndef BSTREE_H_
#define BSTREE_H_
typedef struct abb Abb;

Abb* abb_cria (void);
Abb* abb_insere (Abb* raiz, int val);
Abb* abb_busca (Abb* raiz, int val);
Abb *abb_min(Abb *r);
Abb *abb_prox(Abb *r);
Abb *abb_retira(Abb *r, int chave);
void abb_inorder (Abb *root);
void abb_preorder (Abb *root);
void abb_postorder (Abb *root);
void print_tree (Abb *r, int l);

#endif